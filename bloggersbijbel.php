<?php
/**
 * Plugin Name: Converteer Biblija-links naar bloggersbijbel.nl
 * Plugin URI: http://www.bloggersbijbel.nl
 * Description: Converteert Biblija-links naar bloggersbijbel.nl. Voorkomt nodeloos linken naar inlogscherm NBG voor reli-bloggers.
 * Version: 0.1
 * Author: Erik Drenth
 * Author URI: http://www.staatgeschreven.nl
 * License: GPL2
 */
 
function bloggersbijbel($c) {
	$c = str_replace('href="http://www.biblija.net/biblija.cgi','href="http://bloggersbijbel.nl/',$c);
	return $c;  
}
add_filter('the_content','bloggersbijbel');